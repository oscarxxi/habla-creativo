<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'hablacreativo' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '%_%:8cu`2b=-:hDxJbi7cI^#*ol^Z~j/kd)l~EY4$y0@k Q|zdv5B_%yhuQx?:>h' );
define( 'SECURE_AUTH_KEY',  '=U#?6~U36()koFl^z5t$I%{iRHD5wpZz2e_Mp^3y.xF{wLuHUQxxl6{KXW:y]MTQ' );
define( 'LOGGED_IN_KEY',    '-ptpKgo5ZrMi&.i57u]; $>9i)  &b5lO/ <1|)Y0]38#P&5&CU1D-z[%j;r5`0O' );
define( 'NONCE_KEY',        '}He(UqfxFrbGL3;9sI=]};H j@9#`g/7>57%S/k:F88wJL_G!`mcx|a1}n])D!sf' );
define( 'AUTH_SALT',        'FAR,-GbB<3,~9M9T4<l/s($g;II=J3Zk#TtN6aYI5LE{ !UmK2{j;F@rKgr--D[9' );
define( 'SECURE_AUTH_SALT', 'X$a?`QKx6H[Rf=7DDuNc)U9-q<EUTZ5#c|[M1k}}Py!ir5-o[{Ov={dV[eiK.(Kq' );
define( 'LOGGED_IN_SALT',   '/Yp-GJ=nj]tw{bD+y{QlkG}Bu7S,@C{rj F-9Dk&3MmnJzvvY5u7Z/jG)1Oz6]9=' );
define( 'NONCE_SALT',       'D$&00#B-;[m!FilB 6UZxravU[3+9?wyETqmnSjGK):z.iKZw<g~uGd:)B(J_`}A' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
